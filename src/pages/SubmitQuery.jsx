import React, { useState } from 'react';
import { useAction } from 'wasp/client/operations';
import { submitQuery } from 'wasp/client/operations';
import { Link } from 'wasp/client/router';

const SubmitQueryPage = () => {
  const submitQueryFn = useAction(submitQuery);
  const [question, setQuestion] = useState('');
  const [error, setError] = useState(null);

  const handleSubmit = async () => {
    try {
      await submitQueryFn({ question });
      setQuestion('');
      setError(null);
    } catch (err) {
      setError(err.message);
    }
  };

  return (
    <div className="p-6 max-w-lg mx-auto">
      <h1 className="text-2xl font-bold mb-4">Submit a Question</h1>
      {error && <p className="text-red-500 mb-4">{error}</p>}
      <textarea
        className="w-full p-2 border rounded mb-4"
        placeholder="Type your question about Xiaohongshu here..."
        value={question}
        onChange={(e) => setQuestion(e.target.value)}
      />
      <button
        onClick={handleSubmit}
        className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded mb-4"
      >
        Submit
      </button>
      <div>
        <Link to="/" className="text-blue-500 hover:underline">Go back to Home</Link>
      </div>
    </div>
  );
};

export default SubmitQueryPage;
