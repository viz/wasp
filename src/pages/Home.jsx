import React from 'react';
import { useQuery } from 'wasp/client/operations';
import { getUserQueries } from 'wasp/client/operations';
import { Link } from 'wasp/client/router';

const HomePage = () => {
  const { data: queries, isLoading, error } = useQuery(getUserQueries);

  if (isLoading) return 'Loading...';
  if (error) return `Error: ${error}`;

  return (
    <div className="p-8 bg-gray-50 min-h-screen">
      <h1 className="text-3xl font-bold mb-4">Welcome to RedNoteAI</h1>
      <p className="mb-6">Explore Xiaohongshu functionalities and solve problems with our AI assistant.</p>
      <div className="flex gap-4 mb-8">
        <Link to="/submit-query" className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
          Submit a Query
        </Link>
        <Link to="/previous-questions" className="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded">
          View Previous Questions
        </Link>
      </div>
      <h2 className="text-2xl font-semibold mb-4">Your Recent Queries</h2>
      <div className="bg-white shadow rounded-lg p-4">
        {queries.length === 0 ? (
          <p>You have not submitted any queries yet.</p>
        ) : (
          <ul>
            {queries.map((query, index) => (
              <li key={index} className="border-b py-2">
                <p className="font-medium">Q: {query.question}</p>
                <p className="text-gray-600">A: {query.answer || 'Awaiting answer...'}</p>
              </li>
            ))}
          </ul>
        )}
      </div>
    </div>
  );
}

export default HomePage;
