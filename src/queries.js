import { HttpError } from 'wasp/server'

export const getUserQueries = async (args, context) => {
  if (!context.user) { throw new HttpError(401) }
  return context.entities.Query.findMany({
    where: {
      userId: context.user.id
    },
    select: {
      question: true,
      answer: true
    }
  });
}

export const getAllQueries = async (args, context) => {
  if (!context.user) { throw new HttpError(401) };

  return context.entities.Query.findMany({
    select: {
      id: true,
      question: true,
      answer: true
    }
  });
}
